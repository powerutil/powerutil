.include "core/mode.s"
.include "core/env.s"
.include "load/lma.s"
.include "load/lmbz.s"

    lmbz r3, testsym
    cmplwi r3, 0x78
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value loaded"

.data
testsym:
    .byte 0x78
