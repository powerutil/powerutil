.include "core/mode.s"
.include "core/env.s"
.include "load/lma.s"
.include "load/lmbz.s"

    lmbz r3, testsym, 2
    cmpwi r3, 0x03
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value loaded"

.data
testsym:
    .byte 0x01, 0x02, 0x03, 0x04
