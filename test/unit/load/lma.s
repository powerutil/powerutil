.include "core/mode.s"
.include "core/env.s"
.include "load/lma.s"

    lma r3, testsym
    cmpldi r3, 0xff00
    bne failure
    b success

success:
    succeed
failure:
    fail "Loaded wrong address"

.data
    .offset 0xff00
testsym:
