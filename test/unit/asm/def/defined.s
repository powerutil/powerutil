.include "asm/def.s"

..def testsym, 0x78
.if testsym == 0x78
    succeed
.else
    fail "Symbol not defined"
.endif
