.include "core/mode.s"
.include "core/env.s"
.include "logic/vclr.s"

    vspltisb v2, 0x0f
    vclr v2
    vspltisb v0, 0
    vcmpequb. v19, v2, v0
    bc 12, 24, success  /* cr6_0 set if all elements equal */
    b failure

success:
    succeed
failure:
    fail "Register not cleared"
