.include "core/mode.s"
.include "core/env.s"
.include "logic/clr.s"

    li r3, 0xFF
    clr r3
    cmpdi r3, 0
    beq success
    b failure

success:
    succeed
failure:
    fail "Register not cleared"
