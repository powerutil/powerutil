.include "core/mode.s"
.include "core/env.s"
.include "logic/xxclr.s"

    lis r3, testval@ha
    la r3, testval@l(r3)
    lxvdsx vs33, 0, r3
    xxclr vs33
    vspltisb v0, 0
    vcmpequb. v19, v2, v0
    bc 12, 24, success  /* cr6_0 set if all elements equal */
    b failure

success:
    succeed
failure:
    fail "Register not cleared"

.data
    .p2align 16
testval:
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
