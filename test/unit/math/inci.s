.include "core/mode.s"
.include "core/env.s"
.include "math/inci.s"

    li r3, 5
    inci r3, 5
    cmpdi r3, 10
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
