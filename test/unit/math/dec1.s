.include "core/mode.s"
.include "core/env.s"
.include "math/deci.s"
.include "math/dec1.s"

    li r3, 5
    dec1 r3
    cmpdi r3, 4
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
