.include "core/mode.s"
.include "core/env.s"
.include "math/inc.s"

    li r3, 6
    li r4, 3
    inc r3, r4
    cmpdi r3, 9
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
