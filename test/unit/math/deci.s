.include "core/mode.s"
.include "core/env.s"
.include "math/deci.s"

    li r3, 10
    deci r3, 5
    cmpdi r3, 5
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
