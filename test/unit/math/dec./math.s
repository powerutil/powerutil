.include "core/mode.s"
.include "core/env.s"
.include "math/dec..s"

    li r3, 6
    li r4, 3
    dec. r3, r4
    cmpdi r3, 3
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
