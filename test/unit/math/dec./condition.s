.include "core/mode.s"
.include "core/env.s"
.include "math/dec..s"

    li r3, 6
    li r4, 6
    dec. r3, r4
    cmpdi r3, 0
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect condition"
