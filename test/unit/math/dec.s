.include "core/mode.s"
.include "core/env.s"
.include "math/dec.s"

    li r3, 10
    li r4, 5
    dec r3, r4
    cmpdi r3, 5
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
