.include "core/mode.s"
.include "core/env.s"
.include "math/inc..s"

    li r3, 5
    li r4, 8
    crclr 0
    crclr 1
    crclr 2
    crclr 3
    inc. r3, r4
    bgt success
    b failure

success:
    succeed
failure:
    fail "Incorrect condition"
