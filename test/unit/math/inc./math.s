.include "core/mode.s"
.include "core/env.s"
.include "math/inc..s"

    li r3, 5
    li r4, 8
    inc. r3, r4
    cmpdi r3, 13
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
