.include "core/mode.s"
.include "core/env.s"
.include "math/inci.s"
.include "math/inc1.s"

    li r3, 8
    inc1 r3
    cmpdi r3, 9
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect value"
