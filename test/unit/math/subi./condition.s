.include "core/mode.s"
.include "core/env.s"
.include "math/subi..s"

    li r3, 10
    crclr 0
    crclr 1
    crclr 2
    crclr 3
    subi. r3, r3, 10
    beq success
    b failure

success:
    succeed
failure:
    fail "Incorrect condition"
