.include "core/mode.s"
.include "core/env.s"
.include "math/subi..s"

    li r3, 10
    subi. r3, r3, 5
    cmpdi r3, 5
    bne failure
    b success

success:
    succeed
failure:
    fail "Incorrect result"
