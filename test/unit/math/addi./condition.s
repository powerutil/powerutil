.include "core/mode.s"
.include "core/env.s"
.include "math/addi..s"

    li r3, 0
    crclr 0
    crclr 1
    crclr 2
    crclr 3
    addi. r3, r3, 5
    bgt success
    b failure

success:
    succeed
failure:
    fail "Incorrect condition"
