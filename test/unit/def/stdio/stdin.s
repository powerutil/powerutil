.include "asm/def.s"
.include "def/stdio.s"

.ifndef stdin
    fail "No stdin symbol"
.elseif stdin <> 0
    fail "Incorrect value for stdin"
.endif
