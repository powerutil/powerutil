.include "asm/def.s"
.include "def/stdio.s"

.ifndef stderr
    fail "No stderr symbol"
.elseif stderr <> 2
    fail "Incorrect value for stderr"
.endif
