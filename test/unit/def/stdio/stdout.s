.include "asm/def.s"
.include "def/stdio.s"

.ifndef stdout
    fail "No stdout symbol"
.elseif stdout <> 1
    fail "Incorrect value for stdout"
.endif
