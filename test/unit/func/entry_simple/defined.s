.include "core/mode.s"
.include "core/env.s"
.include "func/entry_simple.s"

entry_simple testfunc
.ifndef testfunc
    fail "Function not defined"
.endif
