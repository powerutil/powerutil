.include "core/mode.s"
.include "core/env.s"
.include "func/entry_simple.s"

/* Simulate callsite setup. */
lis r12, testfunc@ha
la r12, testfunc@l(r12)

entry_simple testfunc
    addis r3, r12, .TOC. - testfunc@ha
    addi r3, r3, .TOC. - testfunc@l
    cmpd r2, r3
    bne failure
    b success

success:
    succeed
failure:
    fail "TOC pointer not correctly initialized"
