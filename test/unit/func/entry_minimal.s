.include "core/mode.s"
.include "core/env.s"
.include "func/entry_minimal.s"

entry_minimal testfunc
.ifndef testfunc
    fail "Function not defined"
.endif
