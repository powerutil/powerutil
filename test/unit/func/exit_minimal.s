.include "core/mode.s"
.include "core/env.s"
.include "func/entry_minimal.s"
.include "func/exit_minimal.s"

    b success

entry_minimal testfunc
exit_minimal testfunc

success:
    succeed
