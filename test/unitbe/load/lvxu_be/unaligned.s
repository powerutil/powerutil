.include "core/mode.s"
.include "core/env.s"
.include "asm/noteq.s"
.include "load/lvxu_be.s"

/* Stage address. */
    lis r3, testval@ha
    la r3, testval@l(r3)
    addi r3, r3, 5

/* Load vector. */
    lvxu_be v2, 0, r3

/* Stage expected result. */
    vspltisb v18, 0x0f
    vspltisb v17, 0x0e
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x0d
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x0c
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x0b
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x0a
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x09
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x08
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x07
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x06
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x05
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x04
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x03
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x02
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x01
    vsldoi v18, v18, v17, 1
    vspltisb v17, 0x00
    vsldoi v18, v18, v17, 1

/* Check result. */
    vcmpequb. v19, v2, v18
    bc 12, 24, success  /* Branch if cr6_0 set, true for all elements equal. */
    b failure

success:
    succeed
failure:
    fail "Incorrect value loaded"

.data
    .p2align 4  /* 16 bytes */
/* Big endian unaligned vector. Starts at offset 5. */
testval:
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x0e, 0x0d
    .byte 0x0c, 0x0b, 0x0a, 0x09, 0x08, 0x07, 0x06, 0x05
    .byte 0x04, 0x03, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
