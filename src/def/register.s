/*
 * @macro define_registers - Define register names
 * Defines register names with prefix in range.
 * @summary def prefix[from-to]
 *
 * Power asm uses plain numbers to refer to registers.
 * Defining prefixed register names increases code clarity.
 *
 * Repeat block iterates numbers being defined.
 * Inner IRP block provides single number as parameter.
 *
 * @internal
 * @type assembly
 *
 * @param [i] (str) + prefix - Register name prefix.
 * @param [i] (ui) + from - Lower bound of range to define.
 * @param [i] (ui) + to - Upper bound of range to define.
 */
.macro define_registers prefix, from, to
.Li = \from
.rept (\to - \from + 1)
    .irp num, %.Li
        ..def \prefix\num, \num
    .endr
    .Li = .Li + 1
.endr
.endm

/* Define register names. */
define_registers  r, 0, 31  /* General purpose. */
define_registers  f, 0, 31  /* Floating point. */
define_registers  v, 0, 31  /* Vector. */
define_registers vs, 0, 63  /* Vector scalar. */
define_registers cr, 0,  7  /* Condition field. */

/* Undefine register names macro. */
.purgem define_registers
