/*
 * @interface powerutil
 *
 * @param [i] ("big"|"little") ?/ endian - Endianness.
 *     Abstracted storage access macros use endianness specified here.
 *     No value disables abstraction layer. Explicit endianness macros
 *     are always available.
 */
.macro powerutil endian
