/*
 * Macro types
 *
 * Code - Emits machine code. Use in a code section.
 * Data - Emits data. Use in a data secion.
 * Assembly - Emits only assembly time constructs. Use anywhere.
 * Transcendent - Handles required section changes. Use anywhere.
 */

/*
 * Data format terms
 *
 *       Byte b     8 bits
 *   Halfword h    16 bits    2 bytes
 *       Word w    32 bits    4 bytes   2 halfwords
 * Doubleword d    64 bits    8 bytes   4 halfwords   2 words
 *   Quadword q   128 bits   16 bytes   8 halfwords   4 words   2 doublewords
 */

/*
 * Function entry available registers
 * TODO: Is r2 available in some circumstances? When no LEP?
 *
 * General: r0 r11-12, any of r3-10 not used for params.
 * Special: ctr.
 * Floating point: f0, any of f1-13 not used for params.
 * Vector: v0-1 v14-19, any of v2-13 not used for params.
 * Vector scalar: low halves of vs0-31.
 * Condition field: cr0-1 cr5-7.
 *
 * Floating point map to high halves of first 32 vector scalar,
 * vector map to entire second 32 vector scalar,
 * so effective availability of full vector scalar is:
 *     vs0 vs32-33 vs46-51,
 *     any of vs1-13 not used for floating point params (in f1-13),
 *     any of vs34-45 not used for vector params (in v2-13).
 */

/*
 * System call protocol
 * TODO: Are any registers undefined after a call?
 *
 * Place the system call number in r0.
 * Place the arguments in r3-9.
 * Execute instruction sc to invoke.
 * Error flag r0 indicates failure. Signed integer. 0 success. -1 failure.
 * Return value is in r3.
 *
 * Input:
 *   r0  System call number
 *   r3  Argument 1
 *   r4  Argument 2
 *   r5  Argument 3
 *   r6  Argument 4
 *   r7  Argument 5
 *   r8  Argument 6
 *   r9  Argument 7
 *
 * Output:
 *   r0  Error flag
 *   r3  Return value
 *
 * Instruction:
 *   sc
 */
