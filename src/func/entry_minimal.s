/*
 * @macro entry_minimal - Minimal function entry
 * Emit minimal function entry sequence.
 * No stack frame. No TOC pointer.
 * Minimizes memory access. Minimizes computation.
 *
 * @type transcendent
 * @section .text
 * @noreg
 *
 * @param [i] (str) + name - Function name.
 *
 * @todo Is there a benefit to leaving out the local entry point?
 */
.macro entry_minimal name:req
/* Enter code section. */
.text
/* Global entry point. */
    .global \name
    .type \name, @function
\name :
/* Local entry point. */
    .localentry \name, . - \name
.endm
