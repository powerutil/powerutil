/*
 * @macro exit_simple - Simple function exit
 * Close function opened with :entry_simple\.
 * No stack frame destruction.
 *
 * @type code
 * @noreg
 *
 * @param [i] (str) + name - Function name.
 *     Must match name given in the paired :entry_simple\.
 */
.macro exit_simple name:req
/* Return to caller. */
    blr
/* Set function size. */
    .size \name, . - \name
.endm
