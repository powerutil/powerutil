/*
 * @macro exit_minimal - Minimal function exit
 * Close function opened with :entry_minimal\.
 * No stack frame destruction.
 *
 * @type code
 * @noreg
 *
 * @param [i] (str) + name - Function name.
 *     Must match name given in the paired :entry_minimal\.
 */
.macro exit_minimal name:req
/* Return to caller. */
    blr
/* Set function size. */
    .size \name, . - \name
.endm
