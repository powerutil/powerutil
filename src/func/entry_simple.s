/*
 * @macro entry_simple - Simple function entry
 * Emit simple function entry sequence.
 * No stack frame. TOC pointer initialized.
 * Minimizes memory access.
 *
 * @type transcendent
 * @section .text
 *
 * Register effects
 *   r2  TOC pointer
 *
 * @param [i] (str) + name - Function name.
 */
.macro entry_simple name:req
/* Enter code section. */
.text
/* Global entry point. */
    .global \name
    .type \name, @function
\name :
/* Prepare TOC pointer. */
/* TODO: Does a local label improve speed here? Used by glibc. */
/* TODO: Can instruction fusion improve speed? Example in ISA spec. */
    addis r2, r12, .TOC. - \name@ha
    addi r2, r2, .TOC. - \name@l
/* Local entry point. */
    .localentry \name, . - \name
.endm
