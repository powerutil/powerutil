/*
 * @macro lxvu - Load vector indexed unaligned
 * Load vector register from potentially unaligned memory.
 * Indexed address specification.
 * Works for aligned and unaligned memory.
 *
 * @type code
 * @abstract endian
 * @undef ri vi vp
 *
 * Register usage
 *   ri  Address calculation
 *   vi  Intermediate load
 *   vp  Permute control
 *
 * @param [o] (vr) + vt - Receives value.
 * @param [i] (0|gr) + ra - Address addend.
 * @param [i] (gr/0) + rb - Address addend.
 * @param [-] (gr/ra) ?=r11 ri - Address calculation.
 * @param [-] (vr/vt) ?=v17 vi - Intermediate load.
 * @param [-] (vr/vt/vi) ?=v18 vp - Permute control.
 *
 * @error If #ri is #ra. Usage conflicts.
 * @error If #vi is #vt. Usage conflicts.
 * @error If #vp is #vt. Usage conflicts.
 * @error If #vp is #vi. Usage conflicts.
 */
.macro lvxu vt:req ra:req rb:req ri=r11 vi=v17 vp=v18
.ifc \endian, big
    lvxu_be \vt, \ra, \rb, \ri, \vi, \vp
.else;.ifc \endian, little
    lvxu_le \vt, \ra, \rb, \ri, \vi, \vp
.else
    .error "lvxu requires param endian"
.endif;.endif
.endm
