/*
 * @macro lvxu_le - Load vector indexed unaligned, little endian
 * @variant lvxu - little endian
 */
.macro lvxu_le vt:req ra:req rb:req ri=r11 vi=v17 vp=v18
.if \rb == r0
    .error "r&b may not be r0"
.endif
..noteq \ri, \ra, "r&i may not be r&a"
..noteq \vi, \vt, "v&i may not be v&t"
..noteq \vp, \vt, "v&p may not be v&t"
..noteq \vp, \vi, "v&p may not be v&i"
/* Load vector 0. */
    lvx \vt, \ra, \rb
/* Prepare shift control. */
    lvsr \vp, \ra, \rb
/* Load vector 1. */
    addi \ri, \rb, 16
    lvx \vi, \ra, \ri
/* Shift into alignment. */
    vperm \vt, \vi, \vt, \vp
.endm
