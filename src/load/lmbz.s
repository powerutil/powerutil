/*
 * @macro lmbz - Load symbol byte and zero
 * Load zero extended byte from symbol into general register.
 * @summary rt = ext0(byte sym.d)
 *
 * @type code
 * @noreg
 *
 * @param [o] (gr) + rt - Receives value.
 * @param [i] (sym) + sym - Symbol to load from. Variable address addend.
 * @param [i] (si16) ?=0 d - Displacement. Byte offset from symbol.
 *     Constant address addend.
 */
.macro lmbz rt:req sym:req d=0
    lma \rt, \sym
    lbz \rt, \d(\rt)
.endm
