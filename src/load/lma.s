/*
 * @macro lma - Load symbol address
 * Load symbol address into general register.
 * @summary rt = &sym
 *
 * @type code
 * @noreg
 *
 * @param [o] (gr) + rt - Receives address.
 * @param [i] (sym) + sym - Symbol to load address of.
 */
.macro lma rt:req sym:req
    lis \rt, \sym@ha
    la \rt, \sym@l(\rt)
.endm
