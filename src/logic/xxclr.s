/*
 * @macro xxclr - Vector scalar clear
 * Clear vector scalar register to 0.
 * @summary vst = 0
 *
 * @type code
 * @noreg
 *
 * @param [o] (vsr) + vst - Register to clear.
 */
.macro xxclr vst:req
    xxlxor \vst, \vst, \vst
.endm
