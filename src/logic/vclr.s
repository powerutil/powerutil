/*
 * @macro vclr - Vector clear
 * Clear vector register to 0.
 * @summary vt = 0
 *
 * @type code
 * @noreg
 *
 * @param [o] (vr) + vt - Register to clear.
 */
.macro vclr vt:req
    vxor \vt, \vt, \vt
.endm
