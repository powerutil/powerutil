/*
 * @macro clr - Clear
 * Clear general register to 0.
 * @summary rt = 0
 *
 * @type code
 * @noreg
 *
 * @param [o] (gr) + rt - Register to clear.
 */
.macro clr rt:req
    xor \rt, \rt, \rt
.endm
