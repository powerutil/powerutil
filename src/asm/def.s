/*
 * @macro ..def - Define internal symbol
 * Define symbol for internal use.
 * Makes symbol local, hidden, and permanent. Redefinitions not possible.
 *
 * @type assembly
 *
 * @param [o] (sym) + name - Symbol name. Receives value.
 * @param [i] (*a) + value - Symbol value.
 */
.macro ..def name:req value:req
    .local \name
    .hidden \name
    .equiv \name, \value
.endm
