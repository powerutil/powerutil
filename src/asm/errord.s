/*
 * ..errord - Error with default
 * Emit assembly error.
 * Use error message if provided, default error message otherwise.
 * For use by macros that take an optional error message.
 *
 * @type assembly
 *
 * @param [i] (str) ?/ message - Error message.
 * @param [i] (str) + default - Default error message.
 *
 * @error Always. With provided message or default.
 */
.macro ..errord message default:req
.ifb \message
    ..error \default
.else
    ..error \message
.endif
.endm
