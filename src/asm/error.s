/*
 * ..error - Error
 * Emit assembly error.
 * Message is required. This eliminates opaque errors with no message.
 * Preferred over the standard error directives.
 *
 * @type assembly
 *
 * @param [i] (str) + message - Error message.
 *
 * @error Always. With provided message.
 */
.macro ..error message:req
    .error \message
.endm
