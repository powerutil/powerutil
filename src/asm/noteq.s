/*
 * @macro ..noteq - Validate not equal numeric
 * Verify 2 values not equal numerically.
 *
 * @type assembly
 *
 * @param [i] (number) + first - First value.
 * @param [i] (number) + second - Second value.
 * @param [i] (str) ?/ message - Error message.
 *
 * @error If #first is equal to #second\.
 */
.macro ..noteq first:req second:req message
.if \first == \second
    ..errord \message, "equal values invalid: \first"
.endif
.endm
