/*
 * @macro inci - Increment immediate
 * Increment general register by immediate value.
 * @summary rt += amount
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr/0) + rt - Register to increment.
 * @param [i] (si16) + amount - Amount to increment.
 *
 * @error If #rt is $r0.
 */
.macro inci rt:req amount:req
.if \rt == r0
    .error "inci cannot operate on r0"
.endif
    addi \rt, \rt, \amount
.endm
