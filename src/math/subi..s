/*
 * @macro subi. - Subtract immediate and record
 * Subtract immediate value from general register.
 * Record comparison with 0.
 * @summary rt = ra - si
 *
 * @type code
 *
 * Register effects
 *   cr0  Compare with 0 result
 *
 * @param [o] (gr) + rt - Receives result.
 * @param [i] (0|gr) + ra - Variable subtrahend.
 * @param [i] (si16) + si - Constant minuend.
 */
.macro subi. rt:req ra:req si:req
    subi \rt, \ra, \si
    cmpdi \rt, 0
.endm
