/*
 * @macro dec1 - Decrement by 1
 * Decrement general register by 1.
 * @summary rt -= 1
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr/0) + rt - Register to decrement.
 *
 * @error If #rt is $r0.
 */
.macro dec1 rt:req
.if \rt == r0
    .error "dec1 cannot operate on r0"
.endif
    deci \rt, 1
.endm
