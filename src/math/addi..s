/*
 * @macro addi. - Add immediate and record
 * Add immediate value to general register.
 * Record comparison with 0.
 * @summary rt = ra + si
 *
 * @type code
 *
 * Register effects
 *   cr0  Compare with 0 result
 *
 * @param [o] (gr) + rt - Receives result.
 * @param [i] (0|gr) + ra - Variable addend.
 * @param [i] (si16) + si - Constant addend.
 */
.macro addi. rt:req ra:req si:req
    addi \rt, \ra, \si
    cmpdi \rt, 0
.endm
