/**
 * @macro inc - Increment
 * Increment general register by amount in general register.
 * @summary rt += ra
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr) + rt - Register to increment.
 * @param [i] (gr) + ra - Amount to increment.
 */
.macro inc rt:req ra:req
    add \rt, \rt, \ra
.endm
