/*
 * @macro dec. - Decrement and record
 * Decrement general register by amount in general register.
 * Record comparison with 0.
 * @summary rt -= ra
 *
 * @type code
 *
 * Register effects
 *   cr0  Compare with 0 result
 *
 * @param [io] (gr) + rt - Register to decrement.
 * @param [i] (gr) + ra - Amount to decrement.
 */
.macro dec. rt:req ra:req
    sub. \rt, \rt, \ra
.endm
