/*
 * @macro deci - Decrement immediate
 * Decrement general register by immediate value.
 * @summary rt -= amount
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr/0) + rt - Register to decrement.
 * @param [i] (si16) + amount - Amount to decrement.
 *
 * @error If #rt is $r0.
 */
.macro deci rt:req amount:req
.if \rt == r0
    .error "deci cannot operate on r0"
.endif
    subi \rt, \rt, \amount
.endm
