/*
 * @macro inc. - Increment and record
 * Increment general register by value in general register.
 * Record comparison with 0.
 * @summary rt += ra
 *
 * @type code
 *
 * Register effects
 *   cr0  Compare with 0 result
 *
 * @param [io] (gr) + rt - Register to increment.
 * @param [i] (gr) + ra - Amount to increment.
 */
.macro inc. rt:req ra:req
    add. \rt, \rt, \ra
.endm
