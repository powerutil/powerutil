/*
 * @macro inc1 - Increment by 1
 * Increment general register by 1.
 * @summary rt += 1
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr/0) + rt - Register to increment.
 *
 * @error If #rt is $r0.
 */
.macro inc1 rt:req
.if \rt == r0
    .error "inc1 cannot operate on r0"
.endif
    inci \rt, 1
.endm
