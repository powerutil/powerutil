/*
 * @macro dec - Decrement
 * Decrement general register by amount in general register.
 * @summary rt -= ra
 *
 * @type code
 * @noreg
 *
 * @param [io] (gr) + rt - Register to decrement.
 * @param [i] (gr) + ra - Amount to decrement.
 */
.macro dec rt:req ra:req
    sub \rt, \rt, \ra
.endm
