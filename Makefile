VPATH = src

head = $(addprefix core/, \
	header info ifaces mode env \
)
tail = $(addprefix core/, \
	ifacee \
)
asm = $(addprefix asm/, \
	def error errord noteq \
)
def = $(addprefix def/, \
	register stdio \
)
func = $(addprefix func/, \
	entry_minimal entry_simple exit_minimal exit_simple \
)
load = $(addprefix load/, \
	lma lmbz lvxu_be lvxu_le lvxu \
)
math = $(addprefix math/, \
	addi. dec dec. dec1 deci inc inc. inc1 inci subi. \
)
logic = $(addprefix logic/, \
	clr vclr xxclr \
)
source = $(addsuffix .s, \
	${head} ${asm} ${def} \
	${func} ${load} \
	${math} ${logic} \
	${tail} \
)

utc_core = $(addprefix core/, \
	header info mode env \
)
utc_asm = $(addprefix asm/, \
	def/defined \
)
utc_def = $(addprefix def/, \
	stdio/stdin stdio/stdout stdio/stderr \
)
utc_func = $(addprefix func/, \
	entry_minimal entry_simple/defined entry_simple/tocpointer \
	exit_minimal exit_simple \
)
utc_load = $(addprefix load/, \
	lma lmbz/nodisp lmbz/disp \
)
utc_math = $(addprefix math/, \
	addi./math addi./condition subi./math subi./condition \
	inc inc./math inc./condition inc1 inci dec dec./math dec./condition \
	dec1 deci \
)
utc_logic = $(addprefix logic/, \
	clr vclr xxclr \
)
unittest_common = $(addprefix unit/, $(addsuffix .s, \
	${utc_core} ${utc_asm} ${utc_def} \
	${utc_func} ${utc_load} \
	${utc_math} ${utc_logic} \
))

utbe_load = $(addprefix load/, \
	lvxu_be/unaligned lvxu_be/aligned \
)
unittest_be = $(addprefix unitbe/, $(addsuffix .s, \
	${utbe_load} \
))

utle_load = $(addprefix load/, \
	lvxu_le/unaligned lvxu_le/aligned \
)
unittest_le = $(addprefix unitle/, $(addsuffix .s, \
	${utle_load} \
))

powerutil.s: ${source}
	mkdir -p build \
	&& cd src \
	&& ../util/combine ${source} \
	> ../build/powerutil.s

.PHONY: unittest_common unittest_be unnittest_le unittest testbe testle test

unittest_common:
	rm -rf build/test
	mkdir -p build/test
	for testsource in $(unittest_common); do \
		powerunit buildtest "$$testsource" \
			-I/opt/powerunit/lib \
			-Itest \
			-Isrc \
			-mpower8 \
		&& prefix=$$(dirname "$$testsource") \
		&& mkdir -p "build/test/$$prefix" \
		&& testbin=$$(echo "$$testsource" \
			| rev \
			| cut -f2- -d'.' \
			| rev) \
		&& mv "a.out" "build/test/$$testbin"; \
	done
	powerunit runtest build/test

unittest_be:
	rm -rf build/test
	mkdir -p build/test
	for testsource in $(unittest_be); do \
		powerunit buildtest "$$testsource" \
			-I/opt/powerunit/lib \
			-Itest \
			-Isrc \
			-mpower8 \
		&& prefix=$$(dirname "$$testsource") \
		&& mkdir -p "build/test/$$prefix" \
		&& testbin=$$(echo "$$testsource" \
			| rev \
			| cut -f2- -d'.' \
			| rev) \
		&& mv "a.out" "build/test/$$testbin"; \
	done
	powerunit runtest build/test

unittest_le:
	rm -rf build/test
	mkdir -p build/test
	for testsource in $(unittest_le); do \
		powerunit buildtest "$$testsource" \
			-I/opt/powerunit/lib \
			-Itest \
			-Isrc \
			-mpower8 \
		&& prefix=$$(dirname "$$testsource") \
		&& mkdir -p "build/test/$$prefix" \
		&& testbin=$$(echo "$$testsource" \
			| rev \
			| cut -f2- -d'.' \
			| rev) \
		&& mv "a.out" "build/test/$$testbin"; \
	done
	powerunit runtest build/test

unittest: unittest_common

testbe: unittest_be
testle: unittest_le
test: unittest
