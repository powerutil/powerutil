Macros
======

.. toctree::
    macro/func
    macro/load
    macro/logic
    macro/math
