File Descriptor
===============

stdin - Standard input
----------------------

Standard input stream. Value 0.

``stdin``

stdout - Standard output
------------------------

Standard output stream. Value 1.

``stdout``

stderr - Standard error
-----------------------

Standard error stream. Value 2.

``stderr``
