Error
=====

..error - Error
---------------

.. function:: ..error message

    | Emit assembly error.
    | Message is required. This eliminates opaque errors with no message.
    | Preferred over the standard error directives.

    | Type assembly.

    :param string message: Error message. Required. Input only.

    :raise Error: Always. With provided message.

..errord - Error with default
-----------------------------

.. function:: ..errord message? default

    | Emit assembly error.
    | Use error message if provided, default error message otherwise.
    | For use by macros that take an optional error message.

    | Type assembly.

    :param string message: Error message. Optional. No default. Input only.
    :param string default: Default error message. Required. Input only.

    :raise Error: Always. With provided message or default.
