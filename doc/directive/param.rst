Parameter Validation
====================

Parameter validation directives raise an error if validation fails.

..noteq - Validate not equal numeric
------------------------------------

.. function:: ..noteq first second message?

    | Verify 2 values not equal numerically.

    | Type assembly.

    :param number first: First value. Required. Input only.
    :param number second: Second value. Required. Input only.
    :param string message: Error message. Optional. No default. Input only.

    :raise Error: If first is equal to second.
