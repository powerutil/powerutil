Symbol
======

..def - Define internal symbol
------------------------------

.. function:: ..def name value

    | Define symbol for internal use.
    | Make symbol local, hidden, and permanent. Redefinitions not possible.

    | Type assembly.

    :param symbol name: Symbol name. Receives value. Required. Output only.
    :param AnyAssembly value: Symbol value. Required. Input only.
