Math
====

addi. - Add immediate and record
--------------------------------

.. function:: addi. rt ra si

    | Add immediate value to general register.
    | Record comparison with 0.
    | Summary: rt = ra + si

    | Type code.

    | Register effects
    |   cr0 - Compare with 0 result

    :param GeneralRegister rt: Receives result. Required. Output only.
    :param 0|GeneralRegister ra: Variable addend. Required. Input only.
    :param SignedInteger16b si: Constant addend. Required. Input only.

dec. - Decrement and record
---------------------------

.. function:: dec. rt ra

    | Decrement general register by amount in general register.
    | Record comparison with 0.
    | Summary: rt -= ra

    | Type code.

    | Register effects
    |   cr0 - Compare with 0 result

    :param GeneralRegister rt: Register to decrement. Required.
        Input and output.
    :param GeneralRegister ra: Amount to decrement. Required. Input only.

dec - Decrement
---------------

.. function:: dec rt ra

    | Decrement general register by amount in general register.
    | Summary: rt -= ra

    | Type code.
    | No implicit register effects.

    :param GeneralRegister rt: Register to decrement. Required.
        Input and output.
    :param GeneralRegister ra: Amount to decrement. Required. Input only.

dec1 - Decrement by 1
---------------------

.. function:: dec1 rt

    | Decrement general register by 1.
    | Summary: rt -= 1

    | Type code.
    | No implicit register effects.

    :param 0|GeneralRegister rt: Register to decrement. Required.
        Input and output.

    :raise Error: If rt is r0.

deci - Decrement immediate
--------------------------

.. function:: deci rt amount

    | Decrement general register by immediate value.
    | Summary: rt -= amount

    | Type code.
    | No implicit register effects.

    :param 0|GeneralRegister rt: Register to decrement. Required.
        Input and output.
    :param SignedInteger16b amount: Amount to decrement. Required. Input only.

    :raise Error: If rt is r0.

inc. - Increment and record
---------------------------

.. function:: inc. rt ra

    | Increment general register by value in general register.
    | Record comparison with 0.
    | Summary: rt += ra

    | Type code.

    | Register effects
    |   cr0 - Compare with 0 result

    :param GeneralRegister rt: Register to increment. Required.
        Input and output.
    :param GeneralRegister ra: Amount to increment. Required. Input only.

inc - Increment
---------------

.. function:: inc rt ra

   | Increment general register by amount in general register.
   | Summary: rt += ra

   | Type code.
   | No implicit register effects.

   :param GeneralRegister rt: Register to increment. Required.
       Input and output.
   :param GeneralRegister ra: Amount to increment. Required. Input only.

inc1 - Increment by 1
---------------------

.. function:: inc1 rt

    | Increment general register by 1.
    | Summary: rt += 1

    | Type code.
    | No implicit register usage.

    :param 0|GeneralRegister rt: Register to increment. Required.
        Input and output.

    :raise Error: If rt is r0.

inci - Increment immediate
--------------------------

.. function:: inci rt amount

    | Increment general register by immediate value.
    | Summary: rt += amount

    | Type code.
    | No implicit register usage.

    :param 0|GeneralRegister rt: Register to increment. Required.
        Input and output.
    :param SignedInteger16b amount: Amount to increment. Required. Input only.

    :raise Error: If rt is r0.

subi. - Subtract immediate and record
-------------------------------------

.. function:: subi. rt ra si

    | Subtract immediate value from general register.
    | Record comparison with 0.
    | Summary: rt = ra - si

    | Type code.

    | Register effects
    |   cr0 - Compare with 0 result

    :param GeneralRegister rt: Receives result. Required. Output only.
    :param 0|GeneralRegister ra: Variable subtrahend. Required. Input only.
    :param SignedInteger16b si: Constant minuend. Required. Input only.
