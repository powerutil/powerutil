Load
====

lma - Load symbol address
-------------------------

.. function:: lma rt sym

    | Load symbol address into general register.
    | Summary: rt = &sym

    | Type code.
    | No implicit register usage.

    :param GeneralRegister rt: Receives address. Required. Output only.
    :param symbol sym: Symbol to load address of. Required. Input only.

lmbz - Load symbol byte and zero
--------------------------------

.. function:: lmbz rt sym d?

    | Load zero extended byte from symbol into general register.
    | Summary: rt = ext0(byte sym.d)

    | Type code.
    | No implicit register usage.

    :param GeneralRegister rt: Receives value. Required. Output only.
    :param symbol sym: Symbol to load from. Variable address addend.
        Required. Input only.
    :param SignedInteger16b d: Displacement. Byte offset from symbol.
        Constant address addend. Optional. Default 0.

lvxu - Load vector indexed unaligned
------------------------------------

.. function:: lvxu vt ra rb ri? vi? vp?

    | Load vector register from potentially unaligned memory.
    | Indexed address specification.
    | Works for aligned and unaligned memory.

    | Type code.
    | Abstracts on ``endian``.
    | Leaves undefined: ri vi vp

    | Register usage
    |   ri  Address calculation
    |   vi  Intermediate load
    |   vp  Permute control

    :param VectorRegister vt: Receives value. Required. Output only.
    :param 0|GeneralRegister ra: Address addend. Required. Input only.
    :param GeneralRegister rb: Address addend. Required. Input only.
        May not be r0.
    :param GeneralRegister ri: Address calculation. May not be ra.
        Optional. Default r11. Internal.
    :param VectorRegister vi: Intermediate load. May not be vt.
        Optional. Default v17. Internal.
    :param VectorRegister vp: Permute control. May not be vt vi.
        Optional. Default vp. Internal.

    :raise Error: If ri is ra. Usage conflicts.
    :raise Error: If vi is vt. Usage conflicts.
    :raise Error: If vp is vt. Usage conflicts.
    :raise Error: If vp is vi. Usage conflicts.

lvxu_be - Load vector indexed unaligned, big endian
---------------------------------------------------

.. function:: lvxu_be vt ra rb ri? vi? vp?

    | Big endian variant of ``lvxu``.


lvxu_le - Load vector indexed unaligned, little endian
------------------------------------------------------

.. function:: lvxu_le vt ra rb ri? vi? vp?

    | Little endian variant of ``lvxu``.
