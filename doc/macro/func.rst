Function
========

entry_minimal - Minimal function entry
--------------------------------------

.. function:: entry_minimal name

    | Emit minimal function entry sequence.
    | No stack frame. No TOC pointer.
    | Minimizes memory access. Minimizes computation.

    | Type transcendent.
    | Changes section to ``.text``.
    | No implicit register usage.

    :param string name: Function name. Required. Input only.

entry_simple - Simple function entry
------------------------------------

.. function:: entry_simple name

    | Emit simple function entry sequence.
    | No stack frame. TOC pointer initialized.
    | Minimizes memory access.

    | Type transcendent.
    | Changes section to ``.text``.

    | Register effects
    |   r2  TOC pointer

    :param string name: Function name. Required. Input only.

exit_minimal - Minimal function exit
------------------------------------

.. function:: exit_minimal name

    | Close function opened with ``entry_minimal``.
    | No stack frame destruction.

    | Type code.
    | No implicit register usage.

    :param string name: Function name.
        Must match name given in the paired ``entry_minimal``.
        Required. Input only.

exit_simple - Simple function exit
----------------------------------

.. function:: exit_simple name

    | Close function opened with ``entry_simple``.
    | No stack frame destruction.

    | Type code.
    | No implicit register usage.

    :param string name: Function name.
        Must match name given in the paired ``entry_simple``.
        Required. Input only.
