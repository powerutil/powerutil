Logic
=====

clr - Clear
-----------

.. function:: clr rt

    | Clear general register to 0.
    | Summary: rt = 0

    | Type code.
    | No implicit register effects.

    :param GeneralRegister rt: Register to clear. Required. Output only.

vclr - Vector clear
-------------------

.. function:: vclr vt

    | Clear vector register to 0.
    | Summary: vt = 0

    | Type code.
    | No implicit register effects.

    :param VectorRegister vt: Register to clear. Required. Output only.

xxclr - Vector scalar clear
---------------------------

.. function:: xxclr vst

    | Clear vector scalar register to 0.
    | Summary: vst = 0

    | Type code.
    | No implicit register effects.

    :param VectorScalarRegister vst: Register to clear. Required. Output only.
