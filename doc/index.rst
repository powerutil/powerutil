.. |avatar| image:: /../img/powerutil.png
    :width: 68
    :height: 68

|avatar| powerutil documentation
================================

| Power Architecture assembly utilities.
| Assembly library for writing Power code.

| Source: https://gitlab.com/powerutil/powerutil
| Bug reports: https://gitlab.com/powerutil/powerutil/issues

**Features**

* Define slim functions
* Unaligned vector load
* Symbolic memory load
* Convenience math and logic operations
* Register name symbols
* Validate macro arguments

**Usage**

.. code-block:: gas

    .include "powerutil.s"
    powerutil endian=big

    /* powerutil API available, with big endian storage access */

**Contents**

.. toctree::
    :maxdepth: 2

    load
    directive
    macro
    symbol
