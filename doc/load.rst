Loading
=======

*powerutil* is a library of assembly code. The library is provided as a single
file for including into your code. An initialization macro takes parameters
through which you specify things like byte endianness for storage access.

To use *powerutil* include the ``powerutil.s`` file and call the ``powerutil``
macro with your desired configuration.

.. code-block:: gas

    .include "powerutil.s"
    powerutil endian=little

    /* powerutil API available, with little endian storage access */

Initialization Parameters
-------------------------

.. function:: powerutil endian?

    | Initialize *powerutil* with specified configuration.

    :param string endian: Byte endianness for storage access.
        Allowed values "big" or "little".
        Optional. No default. Leaving unspecified disables the endianness
        abstraction layer (abstracted macros like ``lvxu`` will be
        unavailable). Explicit endianness macros are always available
        (for example, ``lvxu_be`` or ``lvxu_le``).
